var gulp = require('gulp');
var browserify = require('browserify');
var sass = require('gulp-sass');
var stringify = require('stringify');
var watchify = require('watchify');

var browserSync = require('browser-sync').create();
var source = require('vinyl-source-stream')
var buffer = require('vinyl-buffer');
var assign = require('lodash.assign');
var uglify = require('gulp-uglify');


gulp.task('connect', function () {
	browserSync.init({
        server: {
            baseDir: "./dist"
        },
        port: 8080
    });

    gulp.watch("./sass/*.sass", ['sass']);

	gulp.watch("./js/**", ['js']);
	gulp.watch("./translate/**", ['js']);

    gulp.watch("./dist/*.html", function () {
    	browserSync.reload()
    });
})

gulp.task('sass', function() {
	gulp.src('./sass/*.sass')
	.pipe(sass())
	.pipe(gulp.dest('./dist/css/'))
	.pipe(browserSync.stream())
});

gulp.task('html', function() {
	gulp.src('./dist/*.html')
});



var opts = assign({}, watchify.args, {entries: ['./js/main.js'],});

gulp.task('js', function() {

	watchify(
		browserify(opts)
		.transform(stringify, {
	    	appliesTo: { includeExtensions: ['.html'] }
		})
	)
	.bundle()
	.on('end', (e) => {
		browserSync.reload()
	})
	.pipe(source('main.js'))
	.pipe(buffer())
	//.pipe(uglify())
	.pipe(gulp.dest('./dist/js/'))

});


gulp.task('default', ['connect', 'sass']);
