require('./filter-card-item');
require('./pagination');

angular.module('directives', ['filter-card-item', 'pagination']);