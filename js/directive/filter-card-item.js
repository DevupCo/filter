angular.module('filter-card-item', [])

	.directive('filterCardItem', filterCardItem);



/*=============================
=            Logic            =
=============================*/

/**
 * Filter card template
 * @return {Object}
 */
function filterCardItem() {
	return {
		restrick: 'A',
		replace: true,
		template: require('../template/filter-card-item.html'),
		controller: controller,
		controllerAs: "card",
		bindToController: {
			data: "="
		}
	}
}

/**
 * [controller description]
 * @return {[type]} [description]
 */
function controller() {
	var wm = this;


}