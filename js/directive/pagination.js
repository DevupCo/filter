angular.module('pagination', [])

	.directive('pagination', ['$location', '$anchorScroll', pagination])



/*=============================
=            Logic            =
=============================*/

/**
 * Do paginated data
 * @return {Object}
 */
function pagination() {
	return {
		restrick: 'A',
		replace: true,
		template: require('../template/pagination.html'),
		controller: controller,
		controllerAs: "page",
		bindToController: {
			data: "=",
			active: '='
		}
	}
}

/**
 * [controller description]
 * @return {[type]} [description]
 */
function controller($location, $anchorScroll) {
	var wm = this;

	/**
	 * convert number to array
	 * @type {Array}
	 */
	wm.numToArray = numToArray;

	/**
	 * change current page
	 * @type {undefined}
	 */
	wm.changePage = changePage;


	////////////////////////////////////////////////////

	function numToArray(num) {
		
		return  new Array(num)
	}


	function changePage(num) {

		wm.active = num
		
		// if prev <= 0
		if(num <= 0) {	wm.active = 1 } 

		// if next >= max count
		if(num >= wm.data) { wm.active = wm.data }

		// the element you wish to scroll to.
    	$location.hash('top');

    	// call $anchorScroll()
    	$anchorScroll();
	}

}
