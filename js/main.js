var angular = require('angular')

require('./directive')
require('./controller')
require('./service')

require('angular-translate')
require('angular-material')


/*=================================
=            Bootstrap            =
=================================*/


angular.module('app', [
	'directives', 'services',
	'pascalprecht.translate', 'ngMaterial', 'filter'
])



/*==============================
=            Config            =
==============================*/

.config(['$translateProvider', '$mdDateLocaleProvider', '$compileProvider', function ($translateProvider, $mdDateLocaleProvider, $compileProvider){
	// set location 
	$translateProvider.preferredLanguage('en');
	$translateProvider
		.translations('en', require('../translate/en'))
	$compileProvider.debugInfoEnabled(false);

}])