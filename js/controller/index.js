var app = angular.module('filter', []);


app.controller('filterCtrl', ['$translate', 'Database', '$scope', 'ObjToQuery', '$filter', function($translate, Database, $scope, ObjToQuery, $filter) {
	var wm = this; 

	var filters = false;

	/**
	 * Current page
	 * @type {Number}
	 */
	wm.currentPage = 1;

	wm.currentDropdown = 0;

	/**
	 * Options for filter
	 * @type {Object}
	 */
	wm.options = {
		select: {},
		other: {}
	}

	wm.convertDate = convertDate;
	wm.sendFilter = sendFilter;

	getCard(); 

	getFilters();
	///////////////////////////////////////////////////////////


	/**
	 * When filter.currentPage will be change, get new cards
	 * @param  {Number} value) {
	 * @return {undefined} 
	 */
	$scope.$watch('filter.currentPage', function (value, old) {
		// not change
		if(value === old) {
			return;
		}

		if(filters) {
			var options = ObjToQuery(filters, wm.options.select) 
		} else {
			var options = ObjToQuery(wm.options.select) 
		}


		Database.getPage(value, options).then(function (res) {
			wm.cards = res.data;
		});
	})

	/**
	 * When options will be change, get mew cards
	 * @param  {Objext} value)
	 * @param  {Boolean} true   [Mean that watch an object]
	 * @return {undefined}
	 */
	$scope.$watch('filter.options.select', function (value, old) {
		// if not change
		if(value === old) {
			return;
		}

		// ObjToQuery converts object to query string
		Database.filters( ObjToQuery(wm.options.select)).then(function (res) {
			
			// for each response menu
			for(var drop in res) {
				// eche option
				for(var opt in res[drop]) {
					// if not set then update
					if(!wm.options.select[opt]) {
						wm.dropdown[drop] =  res[drop]
					}
				}
		
			}

		})



	}, true);

	/**
	 * Convert standart date to readable
	 * @param  {Date} date 
	 * @return {String}
	 */
	function convertDate(date) {
		return $filter('date')(date, 'yyyy.MM.dd')
	}

	/**
	 * get options to contert and send to server
	 * @return {undefined} 
	 */
	function sendFilter() {

		filters = angular.copy(wm.options.other)
		console.log(wm.options.select )
		
		getCard( ObjToQuery(wm.options.other, wm.options.select ) );
	}

	/**
	 * get all of card item and set to wm.cards
	 * @return {undefined}
	 */
	function getCard(options) {
		console.log(options)
		Database.card(options).then(function (res) {
			wm.cards = res.data;
			wm.pageCount = res.pageCount
		});
	}

	/**
	 * get all dropdown menu
	 * @return {undefined}
	 */
	function getFilters(options, cb) {
		Database.filters(options).then(function (res) {

			if(cb) {return cb(res)}

			wm.dropdown = res;

		});

	}



}])
