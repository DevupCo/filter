angular.module('db', [])

	.factory('Database', ['$q', '$http', Database]);



/*=============================
=            Logic            =
=============================*/

function Database($q, $http) {
	var url =  'http://62.210.251.109:14000';
	return {
		card: card,
		getPage: getPage,
		filters: filters
	}

	/////////////////////////////////////////////////

	/**
	 * Get cards from Database
	 * @return {Promise}
	 */
	function card(options) {
		var deferred = $q.defer();

		if(options) {
			var path = url + '/data?' + options;
		} else {
			var path = url + '/data';
		}


		$http.get(path).then(function (res) {
			deferred.resolve(res.data)
		})

		return deferred.promise
	}

	/**
	 * Get cards from Database by index
	 * @return {Promise}
	 */
	function getPage(index, options) {

		if(options) {
			var path = url + '/data?page=' + index + '&' + options;
		} else {
			var path = url + '/data?page=' + index ;
		}


		var deferred = $q.defer();

		$http.get(path).then(function (res) {
			deferred.resolve(res.data)
		})

		return deferred.promise
	}


	/**
	 * get and apply any filters
	 * @return {[type]} [description]
	 */
	function filters(query) {
		var deferred = $q.defer();

		if( query) {
			var path = url + '/filters?' + query;
		} else {
			var path = url + '/filters' ;
		}

		$http.get(path).then(function (res) {
			deferred.resolve(res.data)
		})

		return deferred.promise
	}

}