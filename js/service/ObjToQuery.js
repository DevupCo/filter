angular.module('ObjToQuery', [])

	.factory('ObjToQuery', ObjToQuery);



/*=============================
=            Logic            =
=============================*/

function ObjToQuery() {

	return convert;

	////////////////////////////////////////////

	/**
	 * return convert object to query string
	 * @param  {Objext} obj 
	 * @return {String}
	 */
	function convert(obj, obj2) {
		var str = [];
		
		for(var p in obj) {
			if(!obj[p]) {
				continue;
			}

			if (obj.hasOwnProperty(p)) {
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
		}

		if(obj2) {
			for(var p in obj2) {
				if(!obj2[p]) {
					continue ;
				}

				if (obj2.hasOwnProperty(p)) {
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj2[p]));
				}
			}
		}

		return str.join("&");
	}
}